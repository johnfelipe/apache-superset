# Running Superset with Nginx Server and SSL certification on Ubuntu 16.04


## Install python 3.6 on Ubuntu
Note : this part should be run using any sudo user

```
sudo add-apt-repository ppa:jonathonf/python-3.6
sudo apt update
sudo apt install python3.6
sudo apt install python3.6-dev
sudo apt install python3.6-venv
wget https://bootstrap.pypa.io/get-pip.py
sudo python3.6 get-pip.py
sudo ln -s /usr/bin/python3.6 /usr/local/bin/python3
sudo ln -s /usr/local/bin/pip /usr/local/bin/pip3

```


## Getting the first working Superset version up

- Setup the user who will be using the superset
```
#Add User on who's account the superset will run, you will be asked to password and somedetails releated to the new user
sudo adduser flaskuser

# Add sudo privileges to user
sudo usermod -a -G sudo flaskuser

# Change to that user and go to base directory
su flaskuser
cd ~
```
- Add the basic dependencies

```
sudo apt-get install build-essential libssl-dev libffi-dev libsasl2-dev libldap2-dev
```

- Install a virtualenvironment

```
# Install virtualenv module
sudo apt install virtualenv

# Create venv a virtual environment named venv
export LC_ALL=C
export LC_ALL=C.UTF-8
export LANG=C.UTF-8
virtualenv --python=python3.6 venv


# Activate venv
$ . /home/flaskuser/venv/bin/activate

```

- Upgrade pip etc

```
pip install --upgrade setuptools pip

```

- Now do the actual setup and see if it works
- [the below command is instructions from here, please use this link as a reference](https://superset.incubator.apache.org/installation.html)

```
# Important note, don't use sudo on any of the below commands

# Install superset
pip install superset

# Create an admin user (you will be prompted to set username, first and last name before setting a password)
fabmanager create-admin --app superset
# I created my_first_name.last_name user here. note the password that you input here

# Initialize the database
superset db upgrade

# Load some data to play with
superset load_examples

# Create default roles and permissions
superset init

# Start the web server on port 8080 
superset runserver -p 8080
```

- Go the browser and type the ip of the instance. This connects by default to the http port. This should give you superset. Login and see that it works

- Now that you have it setup, don't expect to log back into it until some other steps are done

- Upgrade superset to the latest version

```
pip install --upgrade supserset

```


---

## Getting it up on a domain name registrar (I have used godaddy)
Note: dont use this part if you already have a domain, `dap-udea.org`
- login to [godaddy.com](http://godaddy.in/)
- Setup Account.
- Buy a domain that you like
- Then click on your name on the top right and click on manage my domains
- If using an existing domain, then add an A Host to link to your particular server IP
- If using a new domain, then add @ forwarding to your IP. and then also add www domain forwarding to @
- Once all this is done and takes effect, you should see http://xyz.domain-name.com reflects the front page you saw earlier
---

## Setup some superset essentials

- Take server down by hitting ctrl+C
- In the venv environment check your PYTHONPATH
- [Find your PYTHONPATH](http://www.dummies.com/programming/python/how-to-find-path-information-in-python/)
```
# Open the Python Shell.
$ python


>>> import sys
>>> for p in sys.path:
>>>		print(p)	
>>> exit()
```
- choose one of the paths printed there for saving superset_config.py
- I chose /home/flaskuser/venv/lib/python3.6/site-packages/superset_config.py
```
# Create a new config file
nano /home/flaskuser/venv/lib/python3.6/site-packages/superset_config.py
```
- The base settings of the file are as follows:

```
#---------------------------------------------------------
# Superset specific config
#---------------------------------------------------------
ROW_LIMIT = 5000
SUPERSET_WORKERS = 4
# You will be serving site on port 8000 from gunicorn which sits in front of flask, and then send to nginx
SUPERSET_WEBSERVER_PORT = 8000
#---------------------------------------------------------

#---------------------------------------------------------
# Flask App Builder configuration
#---------------------------------------------------------
# Your App secret key
SECRET_KEY = '\2\mthisismyscretkey\1\2\a\b\y\h'

# The SQLAlchemy connection string to your database backend
# This connection defines the path to the database that stores your
# superset metadata (slices, connections, tables, dashboards, ...).
# Note that the connection information to connect to the datasources
# you want to explore are managed directly in the web UI
SQLALCHEMY_DATABASE_URI = 'sqlite:////home/flaskuser/.superset/superset.db'

# Flask-WTF flag for CSRF
CSRF_ENABLED = True

# Set this API key to enable Mapbox visualizations
MAPBOX_API_KEY = ''
```

- We will be making several changes to this file as we go about modifying some things
---


## Getting Mapbox

- This is a good time to get mapbox api key which is the default configured map on superset
- Login to mapbox.com
- Create an account
- Once logged in go to top right and click on account
- Click on API access tokens and get one
- Add this to your superset_config.py
- The should have this line added:

```
# Set this API key to enable Mapbox visualizations
MAPBOX_API_KEY = 'pk.eyJ1IjoicmFodWwtbWFkZHkiLCJhIjoiY2o0YTF2OTBrMHRxcTJxbzZxcm5zbHV5aiJ9.jVLtVWHVGnRJcqsMa8XiMA'

```
---

## Setting up Nginx

- What we are looking for on nginx is to run a reverse proxy. The actual server is gunicorn sitting in front of flask, which is the framework hosting superset
- Some references for this section: [Nginx with Reverse Proxy ](https://www.nginx.com/resources/admin-guide/reverse-proxy/), [nginx.conf for optimized performance](https://www.linode.com/docs/web-servers/nginx/configure-nginx-for-optimized-performance), [ Setting up postgres nginx and gunicorn ](https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-debian-8 ),  [How to install nginx ](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-16-04)

 
### Install Nginx

- Install commands
```
# Install
sudo apt-get update
sudo apt-get install nginx

# Start and Reload
sudo systemctl start nginx
sudo systemctl reload nginx


# Check status
sudo systemctl status nginx
# It should be running


```
- At this point if you browse to your site, it should show nginx bad gateway.


### nginx conf

- There are two files to change, nginx.conf and the file that it includes in sites-enabled
- Feel free to change the configuration as per requirements of your server
- Open nginx.conf as follows:
```
sudo nano /etc/nginx/nginx.conf
```

- See file below
- If you want to understand what each of these mean go to [Reference ](https://www.linode.com/docs/web-servers/nginx/configure-nginx-for-optimized-performance)

```
user www-data;
worker_processes auto;
pid /run/nginx.pid;

#from linode nginx.config optimizer

events {
    worker_connections 650;
    use epoll;
    multi_accept on;
}

http {
		#from linode nginx.config optimizer
		keepalive_requests 100000;

		sendfile on;
		tcp_nopush on;
		tcp_nodelay on;
		keepalive_timeout 65;
		types_hash_max_size 2048;

		client_header_timeout  3m;
		client_body_timeout    3m;
		send_timeout           3m;

		open_file_cache max=1000 inactive=20s;
		open_file_cache_valid 30s;
		open_file_cache_min_uses 5;
		open_file_cache_errors off;

		gzip on;
		gzip_min_length  1000;
		gzip_buffers     4 4k;
		gzip_types       text/html application/x-javascript text/css application/javascript text/javascript text/plain text/xml applica$
		gzip_disable "MSIE [1-6]\.";

		# [ debug | info | notice | warn | error | crit | alert | emerg ]
		error_log  /var/log/nginx.error_log  warn;

		log_format main      '$remote_addr - $remote_user [$time_local]  '
		  '"$request" $status $bytes_sent '
		  '"$http_referer" "$http_user_agent" '
					'"$gzip_ratio"';

		log_format download  '$remote_addr - $remote_user [$time_local]  '
		  '"$request" $status $bytes_sent '
		  '"$http_referer" "$http_user_agent" '
					'"$http_range" "$sent_http_content_range"';

		map $status $loggable {
			~^[23]  0;
			default 1;
		}

		#-------------from basic conf file-----------

		include /etc/nginx/mime.types;
		default_type application/octet-stream;


		access_log /var/log/nginx/access.log;

		include /etc/nginx/conf.d/*.conf;
		include /etc/nginx/sites-enabled/*;

}


```

- If you notice this includes the files in sites-enabled. For now we should have no sites enabled.
- You can see these files in the sites-enabled folder
```
cd /etc/nginx/sites-enabled/
ls

# If there is anything you can remove as follows
sudo rm file-to-be-removed.conf
```

- Now create a config file in sites-available and hard link it to sites-enabled

```
cd /etc/nginx/sites-available
sudo nano superset.conf
```

- create a really simple config file to start off. We will change this later.
```
server {
        listen   80; 
        server_name dap-udea.org; 

        location / {
			proxy_buffers 16 4k;
            proxy_buffer_size 2k;
            proxy_pass http://127.0.0.1:8000;

        }
}
```

- Now hardlink this superset.conf into the sites-enabled folder
```
# Hardlinking files
sudo ln -s /etc/nginx/sites-available/superset.conf /etc/nginx/sites-enabled

# Test for syntax
sudo nginx -t
sudo systemctl reload nginx
```

- The output should look as follows:

```
#Output
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
``` 

### Test Server

- This is a good time to test whether server works (without the SSL bit)

```
# Login as flaskuser
sudo su flaskuser

# activate virtualenvironment
cd ~
. ./venv/bin/activate

# Start the web server on port 8000 (Note that we have changed the server port) in the background 
superset runserver -p 8000 &
```
- Ensure working from a browser then leave it running cause we will need it 
- If above is not working, go back and ensure all steps followed
---

## Getting SSL Up

- We will be using Let's encrypt for SSL.


### Install ACME client protocol

Note: the below setps should be done with root user, and make sure that the `superset runserver -p 8000` is running

```
curl https://get.acme.sh | sh
```

- you should get an output with the following

```
root@gticset-ubuntu-s-6vcpu-16gb-nyc3-01:~# curl https://get.acme.sh | sh
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   705  100   705    0     0   4085      0 --:--:-- --:--:-- --:--:--  4098
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  164k  100  164k    0     0   939k      0 --:--:-- --:--:-- --:--:--  940k
[Tue Nov 20 12:21:29 -05 2018] Installing from online archive.
[Tue Nov 20 12:21:29 -05 2018] Downloading https://github.com/Neilpang/acme.sh/archive/master.tar.gz
[Tue Nov 20 12:21:29 -05 2018] Extracting master.tar.gz
[Tue Nov 20 12:21:29 -05 2018] It is recommended to install socat first.
[Tue Nov 20 12:21:29 -05 2018] We use socat for standalone server if you use standalone mode.
[Tue Nov 20 12:21:29 -05 2018] If you don't use standalone mode, just ignore this warning.
[Tue Nov 20 12:21:29 -05 2018] Installing to /root/.acme.sh

[Tue Nov 20 12:21:29 -05 2018] Installed to /root/.acme.sh/acme.sh
[Tue Nov 20 12:21:29 -05 2018] Installing alias to '/root/.bashrc'
[Tue Nov 20 12:21:29 -05 2018] OK, Close and reopen your terminal to start using acme.sh
[Tue Nov 20 12:21:29 -05 2018] Installing cron job
no crontab for root
no crontab for root
[Tue Nov 20 12:21:29 -05 2018] Good, bash is found, so change the shebang to use bash as preferred.
[Tue Nov 20 12:21:29 -05 2018] OK
[Tue Nov 20 12:21:29 -05 2018] Install success!

```

- Just issue a cert

```
/root/.acme.sh/acme.sh --issue  -d dap-udea.org  --nginx
```
- you should get the following result

```
root@gticset-ubuntu-s-6vcpu-16gb-nyc3-01:~# /root/.acme.sh/acme.sh --issue  -d dap-udea.org  --nginx
[Tue Nov 20 12:48:23 -05 2018] Registering account
[Tue Nov 20 12:48:23 -05 2018] Registered
[Tue Nov 20 12:48:23 -05 2018] ACCOUNT_THUMBPRINT='1qXoXCyVfIg0ABEFWC7NNvvGFserwNhdPBQJdQfAWNw'
[Tue Nov 20 12:48:23 -05 2018] Creating domain key
[Tue Nov 20 12:48:24 -05 2018] The domain key is here: /root/.acme.sh/dap-udea.org/dap-udea.org.key
[Tue Nov 20 12:48:24 -05 2018] Single domain='dap-udea.org'
[Tue Nov 20 12:48:24 -05 2018] Getting domain auth token for each domain
[Tue Nov 20 12:48:24 -05 2018] Getting webroot for domain='dap-udea.org'
[Tue Nov 20 12:48:24 -05 2018] Getting new-authz for domain='dap-udea.org'
[Tue Nov 20 12:48:24 -05 2018] The new-authz request is ok.
[Tue Nov 20 12:48:24 -05 2018] Verifying:dap-udea.org
[Tue Nov 20 12:48:24 -05 2018] Nginx mode for domain:dap-udea.org
[Tue Nov 20 12:48:24 -05 2018] Found conf file: /etc/nginx/sites-enabled/superset.conf
[Tue Nov 20 12:48:24 -05 2018] Backup /etc/nginx/sites-enabled/superset.conf to /root/.acme.sh/dap-udea.org/backup/dap-udea.org.nginx.conf
[Tue Nov 20 12:48:24 -05 2018] Check the nginx conf before setting up.
[Tue Nov 20 12:48:24 -05 2018] OK, Set up nginx config file
[Tue Nov 20 12:48:24 -05 2018] nginx conf is done, let's check it again.
[Tue Nov 20 12:48:24 -05 2018] Reload nginx
[Tue Nov 20 12:48:29 -05 2018] Success
[Tue Nov 20 12:48:29 -05 2018] Restoring from /root/.acme.sh/dap-udea.org/backup/dap-udea.org.nginx.conf to /etc/nginx/sites-enabled/superset.conf
[Tue Nov 20 12:48:29 -05 2018] Reload nginx
[Tue Nov 20 12:48:29 -05 2018] Verify finished, start to sign.
[Tue Nov 20 12:48:30 -05 2018] Cert success.
-----BEGIN CERTIFICATE-----
MIIFUDCCBDigAwIBAgISA42JpBjvNHRfwpabPfVJr93dMA0GCSqGSIb3DQEBCwUA
MEoxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MSMwIQYDVQQD
ExpMZXQncyBFbmNyeXB0IEF1dGhvcml0eSBYMzAeFw0xODExMjAxNjQ4MzBaFw0x
OTAyMTgxNjQ4MzBaMBcxFTATBgNVBAMTDGRhcC11ZGVhLm9yZzCCASIwDQYJKoZI
hvcNAQEBBQADggEPADCCAQoCggEBANnhoDqhDL9Lj0+pvA2eFGLV4594sjWEzhVu
h+sH4Qj9fAkj2rWnj9yVr1rp024qGnwzt4I2EEuVbJe/R4QWN1lVLddxi2ZLy6Zy
igPvqmRESbEwSjk9jNyqp6eEl7kOkwIjvLo1Jsikm0I6bXzvlD8LnTHrMrshgUK4
fod/FHNdDiUsM0Lap4qewbRp5m09mZG4noR65m4OrC6gr+wmfrYZ9edbTqSzVI+/
HPKqTPffjInJreB77JwKtj9u09mknUi0pf75DjqImfYZlIcIxNA1tarVbo/LQ+M0
J9D1Ih8mJLb4ctQOul0pplnn8kaBzxzqLV2VcQoa/ZmejYuliPECAwEAAaOCAmEw
ggJdMA4GA1UdDwEB/wQEAwIFoDAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUH
AwIwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUk2f712aYTQFX2DW+S/O35f/40dYw
HwYDVR0jBBgwFoAUqEpqYwR93brm0Tm3pkVl7/Oo7KEwbwYIKwYBBQUHAQEEYzBh
MC4GCCsGAQUFBzABhiJodHRwOi8vb2NzcC5pbnQteDMubGV0c2VuY3J5cHQub3Jn
MC8GCCsGAQUFBzAChiNodHRwOi8vY2VydC5pbnQteDMubGV0c2VuY3J5cHQub3Jn
LzAXBgNVHREEEDAOggxkYXAtdWRlYS5vcmcwTAYDVR0gBEUwQzAIBgZngQwBAgEw
NwYLKwYBBAGC3xMBAQEwKDAmBggrBgEFBQcCARYaaHR0cDovL2Nwcy5sZXRzZW5j
cnlwdC5vcmcwggEEBgorBgEEAdZ5AgQCBIH1BIHyAPAAdQB0ftqDMa0zEJEhnM4l
T0Jwwr/9XkIgCMY3NXnmEHvMVgAAAWcyPOIJAAAEAwBGMEQCIC7yNEAEbDaXCfu3
/uyJBmOU/km20aG5ipFWRlGEJYe3AiBIoSrUJUIkw6ttmdp4TEPis2CpvxjFB3kX
3dMuEYnKVAB3AGPy283oO8wszwtyhCdXazOkjWF3j711pjixx2hUS9iNAAABZzI8
4k0AAAQDAEgwRgIhALn5hdfLiMOqdit4ecFtd33249dwXyfwn3g7gWFFUZlMAiEA
rwmHmgPMmBQ+ub7hNFnHFsoGHcQeJEfmXaZdAHCYpqIwDQYJKoZIhvcNAQELBQAD
ggEBABfpa766+oVy63OUIg3/tnoKPCeZF9eX1Xbu+UKs7LJLLXS0n1jKuQq1Vsoe
6t6OdVRDpeSUHdPYVIgIkd7HZdD18iSOc8TnkT8gNk/iar5oW8nN6qcMftZmJdwv
Cd0bFFHkoWMHQCayIrHEAkqkXAdvo9ZGkq35c1cYfGvub2DddoRxrB1vGRPATphL
80st0cDwfYyDsFkvHCP1485sThBJ/ZPMWJkqgpOIvTGbulIKIuyI6fyiBg3LT2Y6
hKjI3XjTm7/L9EgWCF9i/KOTzMYHtN8bo+dRMDnGD4RhTaHBQzkb2mgaCoN7mj6I
yAfC/QWTqwecCAW9DaZBag1+QyU=
-----END CERTIFICATE-----
[Tue Nov 20 12:48:30 -05 2018] Your cert is in  /root/.acme.sh/dap-udea.org/dap-udea.org.cer 
[Tue Nov 20 12:48:30 -05 2018] Your cert key is in  /root/.acme.sh/dap-udea.org/dap-udea.org.key 
[Tue Nov 20 12:48:31 -05 2018] The intermediate CA cert is in  /root/.acme.sh/dap-udea.org/ca.cer 
[Tue Nov 20 12:48:31 -05 2018] And the full chain certs is there:  /root/.acme.sh/dap-udea.org/fullchain.cer 

```

- The certificate is now installed in /root/.acme.sh/dap-udea.org/ as mentioned the pervious message
```
ls /root/.acme.sh/dap-udea.org/
```

- Now we will update Nginx configuration to add the certificate and make our website listen to port 443

- run the following command `sudo nano /etc/nginx/sites-available/superset.conf` and copy and paste the below configuration

```
server {
	listen 80 default_server;
	listen [::]:80 default_server;
	server_name dap-udea.org;
	return 301 https://$server_name$request_uri;
}

server {
    listen 443 ssl;

	ssl_certificate /root/.acme.sh/dap-udea.org/fullchain.cer;
	ssl_certificate_key /root/.acme.sh/dap-udea.org/dap-udea.org.key;

	# from https://cipherli.st/
	# and https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html

	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
	ssl_prefer_server_ciphers on;
	ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
	ssl_ecdh_curve secp384r1;
	ssl_session_cache shared:SSL:10m;
	ssl_session_tickets off;
	ssl_stapling on;
	ssl_stapling_verify on;
	resolver 8.8.8.8 8.8.4.4 valid=300s;
	resolver_timeout 5s;
	# Disable preloading HSTS for now.  You can use the commented out header line that includes
	# the "preload" directive if you understand the implications.
	#add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";
	add_header Strict-Transport-Security "max-age=63072000; includeSubdomains";
	add_header X-Frame-Options DENY;
	add_header X-Content-Type-Options nosniff;



    # Uncomment this line only after testing in browsers,
    # as it commits you to continuing to serve your site over HTTPS
    # in future
    add_header Strict-Transport-Security "max-age=31536000";

    access_log /var/log/nginx/sub.log combined;

    server_name dap-udea.org;

	location /.well-known {
		alias /var/www/html/.well-known;
	}

    location / {
			proxy_buffers 16 4k;
			proxy_buffer_size 2k;
			proxy_pass http://127.0.0.1:8000;
			#from linode nginx optimizer
			proxy_set_header   Host             $host;
			proxy_set_header   X-Real-IP        $remote_addr;
			proxy_set_header  X-Forwarded-For  $proxy_add_x_forwarded_for;
			proxy_connect_timeout      90;
			proxy_send_timeout         90;
			proxy_read_timeout         90;
			proxy_busy_buffers_size    4k;
			proxy_temp_file_write_size 4k;
			proxy_temp_path            /etc/nginx/proxy_temp;
    }

}

```
- Reload nginx after change
```
sudo nginx -s reload
```

- Now try to access dap-udea.org, you should be able to see that you already have a certificate and the url became https://dap-udea.org/login/


## Notes to start Production Server

- Now we are good to start the server proper
- For this we use the application screen. This helps in persistent sessions. ie those that don't go off when you log off from the bash running the server
- Reference [Persistent SSH Sessions ](https://unix.stackexchange.com/questions/479/keep-ssh-sessions-running-after-disconnection)
- Run screen
```
# Run Screen
screen
>Press enter
> New terminal session

# Change user to flaskuser
sudo su flaskuser

# Go to home directory
cd ~

# Activate virtual environment
. ./venv/bin/activate

# Initialize the database
superset db upgrade
# Ensure everything goes right

# Load some data to play with
superset load_examples
# Ensure everything goes right

# Create default roles and permissions
superset init
# Ensure everything goes right

# Start the web server on port 8000
superset runserver -p 8000

# ctrl a+d to log off screen
# you can type screen -r to log back into screen running server
```

- Now your server is running with  SSL, and Nginx!
---


## Notes to stop Superset

- check if it is running or not using the following command:
```
sudo netstat -ntlup |grep 8000
```
- note in the above command i used port 8000 cause that is the port i used in to run superset, the output of the command should be something like this 
```
root@gticset-ubuntu-s-6vcpu-16gb-nyc3-01:~# netstat -ntlup |grep 8000
tcp        0      0 0.0.0.0:8000            0.0.0.0:*               LISTEN      23107/python3.6 

```
- then you can kill this using the following command, note that the process id is typed in the previous command output `23107`
```
sudo kill 23107
```